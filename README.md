# Certificate Plugin for Netbox

A Netbox plugin for Certificate management.

## Installation

You can follow [the official plugins installation
instructions](https://docs.netbox.dev/en/stable/plugins/#installing-plugins).

If needed, source your Netbox's virtual environment and install the plugin like
a package. We assume [you have already installed
Netbox](https://docs.netbox.dev/en/stable/installation/) and its source code are
in `/opt/netbox`:

```bash
cd /opt/netbox
python3 -m venv venv # if virtual env was not created before
source /opt/netbox/venv/bin/activate
pip install netbox-docker-plugin
```

Enable the plugin in the `/opt/netbox/netbox/netbox/configuration.py` file:

```python
PLUGINS = [
    'netbox_cert_plugin',
]
```

Then, run migrations:

```bash
cd /opt/netbox
python3 netbox/manage.py migrate
```

### Alternative

Another way to install Netbox is to use the [Official netbox-docker
project](https://github.com/netbox-community/netbox-docker).

With this alternate way, you can [customize your Netbox image](https://github.com/netbox-community/netbox-docker/wiki/Using-Netbox-Plugins) and migrations will be
automatically execute each time you restart the container.

## Contribute

### Install our development environment
> Requirements:
> * Python 3.11 & Poetry (See [Python best practices](https://code.europa.eu/digit-c4/dev/python-best-practices))
> * Docker
>
> For production deployment, or users without docker, the backend
> services should be installed according to [Netbox install guide](https://github.com/netbox-community/netbox/blob/master/docs/installation/index.md) and configured
> using the dev environment settings
> * PostgreSQL 15
>   * username: username (with database creation right)
>   * password: password
>   * database: netbox
>   * port: 5432
> * Redis 7.2
>   * port: 6379

Init the project according to [best practices](https://code.europa.eu/digit-c4/dev/best-practices)
```bash
PROJECT_HOME="${HOME}/Workspace/code.europa.eu/digit-c4/netbox/netbox-rps-plugin"
mkdir -p $PROJECT_HOME
git clone git@code.europa.eu:digit-c4/netbox/netbox-cert-plugin.git $PROJECT_HOME \
  && cd $PROJECT_HOME
```

Netbox core application is unfortunately not distributed (yet) in PyPi. Its code must therefore
be imported as an external library in `/lib` using git.
```shell
mkdir -p lib
git clone --branch v3.6.9 https://github.com/netbox-community/netbox.git lib/netbox
```

Init python venv according to [Python best practices](https://code.europa.eu/digit-c4/dev/python-best-practices) then install both Netbox core and this module dependencies
*(use local Python venv instead of messing up with /opt root dir as advised by official tutorial)*
```bash
python3 -m venv venv && source venv/bin/activate
pip install -r lib/netbox/requirements.txt
# If you do not see the poetry installation in the requirements, you need to add manually with the next line.
pip install poetry
poetry install
```

Start a local PostgreSQL and Redis DB *(using docker instead of going through the awful local install of DBs advised by official tutorial)*
```bash
docker compose up -d
```

`netbox-nms` CLI will load Netbox Configuration from `.env` file *(instead of dirty manual modification of core source code)*.
The `.env.example` file matches the configuration for contributing. But any other setting can be provided here to match your setting.
```bash
cp .env.example .env
```

Run database migrations:

```bash
netbox-nms migrate
# Or use the Poetry-less command
python -m netbox_nms_common.manage migrate
# Or directly use the manage.py script in Netbox core
python lib/netbox/netbox/manage.py migrate
```

Create a Netbox super user:

```bash
netbox-nms createsuperuser
# Or use the Poetry-less command
python -m netbox_nms_common.manage createsuperuser
# Or directly use the manage.py script in Netbox core
python lib/netbox/netbox/manage.py createsuperuser
```

Start the Netbox instance:

```bash
netbox-nms runserver 0.0.0.0:8000 --insecure
# Or use the Poetry-less command
python -m netbox_nms_common.manage runserver 0.0.0.0:8000 --insecure
# Or directly use the manage.py script in Netbox core
python lib/netbox/netbox/manage.py runserver 0.0.0.0:8000 --insecure
```

Visit http://localhost:8000/

### Run tests

After installing you development environment, you can run the tests plugin (you
don't need to start the Netbox instance):

```bash
netbox-nms test tests --keepdb -v 2
# Or directly use the manage.py script in Netbox core
python lib/netbox/netbox/manage.py test tests --keepdb -v 2
```

If you want to recreate the testing database remove the `--keepdb` flag from the
command above.

With code coverage, install [coverage.py](https://coverage.readthedocs.io/en/7.3.2/) and use it:

```bash
poetry install --with test
```

The run the test with coverage.py and print the report:

```bash
coverage run --include='*/src/netbox_cert_plugin/*' lib/netbox/netbox/manage.py test tests -v 2
coverage report -m
```
