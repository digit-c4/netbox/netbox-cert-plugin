"""Container Test Case"""
# pylint: disable=duplicate-code

from datetime import datetime
from utilities.testing import APIViewTestCases
from django.contrib.contenttypes.models import ContentType
from users.models import ObjectPermission
from netbox_cert_plugin.models import CertificateAuthority
from tests.base import BaseAPITestCase
#from rest_framework import status   # pylint: disable=E0401

class CertificateAuthorityApiTestCase(
    BaseAPITestCase,
    APIViewTestCases.GetObjectViewTestCase,
    APIViewTestCases.ListObjectsViewTestCase,
    APIViewTestCases.CreateObjectViewTestCase,
    APIViewTestCases.UpdateObjectViewTestCase,
    APIViewTestCases.DeleteObjectViewTestCase,
):

    """Certificate Test Case Class"""

    model = CertificateAuthority
    brief_fields = [
        "acme_url",
        "ca_name",
        "default_validity",
        "end_date",
        "id",
        "key_vault_url",
        "start_date",
        "url"
    ]

    @classmethod
    def setUpTestData(cls) -> None:
        """Class Method"""

        CertificateAuthority.objects.create(ca_name="truc00.com")
        CertificateAuthority.objects.create(ca_name="truc01.com")
        CertificateAuthority.objects.create(ca_name="truc02.com")

        cls.create_data = [
            {
                "ca_name": "test",
                "acme_url": "test acmeurl",
                "key_vault_url": "key vault url",
                "start_date": datetime(2024, 1, 12).date(),
                "end_date": datetime(2024, 10, 10).date(),
                "default_validity": 100,
            },
            {
                "ca_name": "test1",
                "acme_url": "test acmeurl",
                "key_vault_url": "key vault url",
                "start_date": datetime(2024, 1, 12).date(),
                "end_date": datetime(2024, 10, 10).date(),
                "default_validity": 101,
            },
            {
                "ca_name": "test111",
                "acme_url": "test acmeurl",
                "key_vault_url": "key vault url",
                "start_date": datetime(2024, 1, 12).date(),
                "end_date": datetime(2024, 10, 10).date(),
                "default_validity": 111,
            },
        ]
        cls.form_data = {
            "ca_name": "test",
                "acme_url": "test acmeurl",
                "key_vault_url": "key vault url",
                "start_date": datetime(2024, 1, 1).date(),
                "end_date": datetime(2024, 10, 10).date(),
                "default_validity": 100,
        }
    def test_create_authority_with_bad_dates(self):
        """Test to create with the wrong state value"""
        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        obj_perm.users.add(self.user)  # pylint: disable=E1101
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))  # pylint: disable=E1101

        # Modify the form data to have an incorrect order of dates
        form_data = self.form_data.copy()
        form_data['end_date'] = datetime(2024, 1, 10).date()  # End date before start date
        form_data['start_date'] = datetime(2024, 1, 12).date()

        # Try POST with modified form data
        response = self.client.post(self._get_list_url(), form_data, format="json", **self.header)

        # Assert that the response status code is 400 or 422
        self.assertIn(response.status_code, [400, 422])

    def test_create_authority_with_name_already_used(self):
        """Test to create with the name already in use"""
        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        obj_perm.users.add(self.user)  # pylint: disable=E1101
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))  # pylint: disable=E1101

        # Use the form as base of the data to test
        form_data = self.form_data.copy()
        # Try POST with form data
        self.client.post(self._get_list_url(), form_data, format="json", **self.header)
        response = self.client.post(self._get_list_url(), form_data, format="json", **self.header)

        # Assert that the response status code is 400 or 422
        self.assertIn(response.status_code, [400, 422])
