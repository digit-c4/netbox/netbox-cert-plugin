"""Container Views Test Case"""

from utilities.testing import ViewTestCases
from tests.base import BaseModelViewTestCase
from netbox_cert_plugin.models import Certificate


class CertificateViewsTestCase(
    BaseModelViewTestCase,
    ViewTestCases.GetObjectViewTestCase,
    ViewTestCases.GetObjectChangelogViewTestCase,
    ViewTestCases.CreateObjectViewTestCase,
    ViewTestCases.EditObjectViewTestCase,
    ViewTestCases.DeleteObjectViewTestCase,
    ViewTestCases.ListObjectsViewTestCase,
    ViewTestCases.BulkImportObjectsViewTestCase,
    ViewTestCases.BulkDeleteObjectsViewTestCase,
):
    """Certificate Views Test Case Class"""

    model = Certificate

    @classmethod
    # pylint: disable=invalid-name
    def setUpTestData(cls):
        """Class Method"""
        cert1 = Certificate.objects.create(cn="truc00.com")
        cert2 = Certificate.objects.create(cn="truc01.com")

        cls.form_data = {
            "cn": "truc07.com",
            "expiration_time": "1y",
            "ca": "letsencrypt",
            # "state": "To be created",
            "vault_url":"http://www.someurl.com",
            "content": "something",
            "service": "WIFI",
        }

        cls.csv_data = (
            "cn,ca,expiration_time,state,vault_url,content,service",
            "truc04.com,letsencrypt,1m,valid,http://www.someurl.com,something,WIFI",
            "truc05.com,commissign,1y,valid,http://www.someurl.com,something,WIFI",
            "truc06.com,other,1y,valid,http://www.someurl.com,something,WIFI",
            "truc08.com,letsencrypt,1m,valid,http://www.someurl.com,something,",
        )

        cls.csv_update_data = (
            "id,expiration_time,content,vault_url,service",
            f"{cert1.pk},1m,test-content-1,http://mytestvault1.com,LB",
            f"{cert2.pk},1y,test-content-2,http://mytestvault2.com,",
        )

    def tearDown(self) -> None:# pylint: disable=invalid-name
        """Method called immediately after the test method has been called and the result recorded."""
        Certificate.objects.all().delete()
        super().tearDown()
