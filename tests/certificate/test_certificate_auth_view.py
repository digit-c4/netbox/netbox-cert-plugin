"""Container Views Test Case"""

from datetime import datetime
from utilities.testing import ViewTestCases
from django.contrib.contenttypes.models import ContentType
from users.models import ObjectPermission
from netbox_cert_plugin.models import CertificateAuthority
from tests.base import BaseModelViewTestCase



class CertificateAuthorityViewsTestCase(
    BaseModelViewTestCase,
    ViewTestCases.GetObjectViewTestCase,
    ViewTestCases.CreateObjectViewTestCase,
    ViewTestCases.GetObjectChangelogViewTestCase,
    ViewTestCases.EditObjectViewTestCase,
    ViewTestCases.DeleteObjectViewTestCase,
    ViewTestCases.ListObjectsViewTestCase,
    ViewTestCases.BulkImportObjectsViewTestCase,
    ViewTestCases.BulkDeleteObjectsViewTestCase,
):
    """Certificate Authority Views Test Case Class"""

    model = CertificateAuthority

    @classmethod
    # pylint: disable=invalid-name
    def setUpTestData(cls):
        """Class Method"""
        cert_auth1 = CertificateAuthority.objects.create(ca_name="truc00.com")
        cert_auth2 = CertificateAuthority.objects.create(ca_name="truc01.com")
        cert_auth3 = CertificateAuthority.objects.create(ca_name="truc02.com")

        cls.form_data = {
            "ca_name": "Unique Name",
            "acme_url": "c_auth1acme_url",
            "key_vault_url": "c_auth1key_vault_url",
            "start_date": datetime(2024, 10, 10).date(),
            "end_date": datetime(2024, 12, 12).date(),
            "default_validity": 105,
        }

        cls.csv_data = (
            "ca_name,acme_url,key_vault_url,start_date,end_date,default_validity",
            "test1,acme_url1,key_vault_url1,2024-1-31,2024-7-31,10",
            "test2,acme_url2,key_vault_url2,2024-1-31,2024-7-31,11",
            "test3,acme_url3,key_vault_url3,2024-1-31,2024-7-31,12",
        )

        cls.csv_update_data = (
            "id,default_validity",
            f"{cert_auth1.pk},20",
            f"{cert_auth2.pk},22",
            f"{cert_auth3.pk},21",
        )

        cls.form_data = {
            "ca_name": "c_auth5",
            "acme_url": "c_auth1acme_url",
            "key_vault_url": "c_auth1key_vault_url",
            "start_date": datetime(2024, 1, 31).date(),
            "end_date": datetime(2024, 7, 31).date(),
            "default_validity": 105,
        }

        cls.csv_data = (
            "ca_name,acme_url,key_vault_url,start_date,end_date,default_validity",
            "test4,acme_url1,key_vault_url1,2024-1-31,2024-7-31,10",
            "test5,acme_url2,key_vault_url2,2024-1-31,2024-7-31,11",
            "test6,acme_url3,key_vault_url3,2024-1-31,2024-7-31,12",
        )

        cls.csv_update_data = (
            "id,key_vault_url",
            f"{cert_auth1.pk},string1",
            f"{cert_auth2.pk},string2",
            f"{cert_auth3.pk},string3",
        )
    def test_create_authority_with_name_already_used(self):
        """Test to create with the name already in use through the view"""
        # Add object-level permission to allow adding/changing/viewing the object
        obj_perm = ObjectPermission(
            name="Unique Name",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        obj_perm.users.add(self.user)  # pylint: disable=E1101
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))  # pylint: disable=E1101

        # Use the form as base of the data to test
        form_data = {
            "ca_name": "c_auth5",
            "acme_url": "c_auth1acme_url",
            "key_vault_url": "c_auth1key_vault_url",
            "start_date": datetime(2024, 1, 31).date(),
            "end_date": datetime(2024, 7, 31).date(),
            "default_validity": 105,
        }

        self.client.post(self._get_url('add'), form_data)

        response2 = self.client.post(self._get_url('add'), form_data)

        self.assertFormError(response2, 'form', 'ca_name', 'Certificate authority with this CA Name already exists.')

    def test_create_authority_with_enddate_before_startdate(self):
        """Test to create with the name already in use through the view"""
        # Add object-level permission to allow adding/changing/viewing the object
        obj_perm = ObjectPermission(
            name="Unique Name",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        obj_perm.users.add(self.user)  # pylint: disable=E1101
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))  # pylint: disable=E1101

        # Use the form as base of the data to test
        form_data = {
            "ca_name": "c_auth5",
            "acme_url": "c_auth1acme_url",
            "key_vault_url": "c_auth1key_vault_url",
            "start_date": datetime(2024, 7, 31).date(),
            "end_date": datetime(2024, 1, 31).date(),
            "default_validity": 105,
        }

        response1 = self.client.post(self._get_url('add'), form_data)

        form = response1.context['form']
        self.assertIn('The end date must be after the start date', form.errors['__all__'][0])
