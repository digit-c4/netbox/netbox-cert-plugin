"""Container Test Case"""
import json
from utilities.testing import APIViewTestCases
from users.models import ObjectPermission
from django.contrib.contenttypes.models import ContentType
from rest_framework import status   # pylint: disable=E0401
from netbox_cert_plugin.models import Certificate
from tests.base import BaseAPITestCase

def generate_string(character, length):
    """Function to simulate PEM Cert of given length"""
    return character * length
CONTENT = generate_string('a', 5000)
WRONG_CONTENT = generate_string('a', 32001)
SERVICE = 'WIFI'
WRONG_SERVICE = 'test'

class CertificateApiTestCase(
    BaseAPITestCase,
    APIViewTestCases.GetObjectViewTestCase,
    APIViewTestCases.ListObjectsViewTestCase,
    APIViewTestCases.CreateObjectViewTestCase,
    APIViewTestCases.UpdateObjectViewTestCase,
    APIViewTestCases.DeleteObjectViewTestCase,
):
    """Certificate Test Case Class"""

    model = Certificate
    brief_fields = [
        "alt_name",
        "ca",
        "cert_created_at",
        "cert_expired_at",
        "cn",
        "content",
        "expiration_time",
        "id",
        "service",
        "state",
        "url",
        "vault_url",
    ]

    @classmethod
    # pylint: disable=invalid-name
    def setUpTestData(cls) -> None:
        """Class Method"""
        Certificate.objects.create(cn="truc00.com", state="to_be_created")
        Certificate.objects.create(cn="truc01.com", state="to_be_created")
        Certificate.objects.create(cn="truc02.com", state="to_be_created")

        cls.create_data = [
            {"cn": "truc03.com", "ca": "letsencrypt", "expiration_time": "1m", "state": "to_be_created"},
            {"cn": "truc04.com", "ca": "letsencrypt", "expiration_time": "1m", "state": "to_be_created"},
            {"cn": "truc05.com", "ca": "commissign", "expiration_time": "1m", "state": "to_be_created"},
            {"cn": "truc06.com", "ca": "other", "expiration_time": "1m", "state": "to_be_created"},
            {"cn": "truc07.com", "ca": "other", "expiration_time": "1m",
                "content": "test-content", "vault_url": "http://mytestvault.com", "state": "to_be_created", "service": "WIFI"},
        ]

        cls.form_data = {
            "cn": "truc03.com",
            "expiration_time": "1y",
            "ca": "letsencrypt",
            "state": "to_be_created"
        }

        cls.form_data_workflow = {
            "cn": "truc04.com",
            "expiration_time": "1y",
            "ca": "letsencrypt",
            "state": "to_be_created"
        }

        cls.form_data_renewed_change = {
            "cn": "truc05.com",
            "expiration_time": "1y",
            "ca": "letsencrypt",
            "state": "to_be_created"
        }

    def test_create_with_wrong_state_value(self):
        """Test to create with the wrong state value"""
        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        obj_perm.users.add(self.user)  # pylint: disable=E1101
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))  # pylint: disable=E1101

        # Modify the form data to have an incorrect state value
        form_data = self.form_data.copy()
        form_data['state'] = 'toto'  # Incorrect state value

        # Try POST with modified form data
        response = self.client.post(self._get_list_url(), form_data, format="json", **self.header)

        # Assert that the response status code is 400 or 422
        self.assertIn(response.status_code, [400, 422])

    def test_create_with_valid_state_value(self):
        """Test to create with the wrong state value"""
        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        obj_perm.users.add(self.user)  # pylint: disable=E1101
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))  # pylint: disable=E1101

        # Modify the form data to have an incorrect state value
        form_data = self.form_data.copy()
        form_data['state'] = 'to_be_created'  # Incorrect state value

        # Try POST with modified form data
        response = self.client.post(self._get_list_url(), form_data, format="json", **self.header)

        # Assert that the response status code is 201
        self.assertIn(response.status_code, [201])

    def test_edit_with_wrong_state_value(self):
        """Test to edit with the wrong state value"""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        obj_perm.users.add(self.user)  # pylint: disable=E1101
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))  # pylint: disable=E1101

        # Modify the form data to have a correct state value
        form_data = self.form_data.copy()
        form_data['state'] = 'to_be_created'  # Correct state value

        # Try POST with modified form data
        response = self.client.post(self._get_list_url(), form_data, format="json", **self.header)

        certificate = json.loads(response.content)
        form_data['state'] = 'toto'  # Incorrect state value

        response = self.client.patch(self._get_list_url() + f"{certificate['id']}/", form_data, format="json", **self.header)

        # Assert that the response status code is 400 or 422
        self.assertIn(response.status_code, [400, 422])

    def test_workflow_status(self):
        """Test to change state from to_be_created to creating"""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        obj_perm.users.add(self.user)  # pylint: disable=E1101
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))  # pylint: disable=E1101

        # Modify the form data to have a correct state value
        form_data = self.form_data_workflow
        form_data['state'] = 'to_be_created'  # Correct state value

        # Try POST with modified form data
        response = self.client.post(self._get_list_url(), form_data, format="json", **self.header)

        certificate = json.loads(response.content)
        form_data['state'] = 'creating'  # Correct state value
        response = self.client.patch(self._get_list_url() + f"{certificate['id']}/", form_data, format="json", **self.header)

        form_data['state'] = 'errored'  # Correct state value
        response = self.client.patch(self._get_list_url() + f"{certificate['id']}/", form_data, format="json", **self.header)

        form_data['state'] = 'valid'  # Correct state value
        response = self.client.patch(self._get_list_url() + f"{certificate['id']}/", form_data, format="json", **self.header)

        form_data['state'] = 'to_be_renewed'  # Correct state value
        response = self.client.patch(self._get_list_url() + f"{certificate['id']}/", form_data, format="json", **self.header)

        form_data['state'] = 'renewing'  # Correct state value
        response = self.client.patch(self._get_list_url() + f"{certificate['id']}/", form_data, format="json", **self.header)

        form_data['state'] = 'valid'  # Correct state value
        response = self.client.patch(self._get_list_url() + f"{certificate['id']}/", form_data, format="json", **self.header)
        # Assert that the response status code is 200
        self.assertIn(response.status_code, [200])

    def test_renewed_change(self):
        """Test to renew any change"""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        obj_perm.users.add(self.user)  # pylint: disable=E1101
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))  # pylint: disable=E1101

        # Modify the form data to have a correct state value
        form_data = self.form_data_renewed_change
        form_data['state'] = 'to_be_created'  # Correct state value

        # Try POST with modified form data
        response = self.client.post(self._get_list_url(), form_data, format="json", **self.header)

        certificate = json.loads(response.content)
        form_data['state'] = 'creating'  # Correct state value
        response = self.client.patch(self._get_list_url() + f"{certificate['id']}/", form_data, format="json", **self.header)

        form_data['state'] = 'errored'  # Correct state value
        response = self.client.patch(self._get_list_url() + f"{certificate['id']}/", form_data, format="json", **self.header)

        form_data['state'] = 'valid'  # Correct state value
        response = self.client.patch(self._get_list_url() + f"{certificate['id']}/", form_data, format="json", **self.header)

        form_data['state'] = 'to_be_renewed'  # Correct state value
        response = self.client.patch(self._get_list_url() + f"{certificate['id']}/", form_data, format="json", **self.header)

        form_data['state'] = 'to_be_renewed'  # Correct state value
        form_data['alt_name'] = ['test']
        response = self.client.patch(self._get_list_url() + f"{certificate['id']}/", form_data, format="json", **self.header)
        self.assertHttpStatus(response, status.HTTP_200_OK)

    def test_errored_to_renewed(self):
        """Test to renew any change"""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        obj_perm.users.add(self.user)  # pylint: disable=E1101
        obj_perm.object_types.add(     # pylint: disable=E1101
            ContentType.objects.get_for_model(self.model)
        )

        form_data = self.form_data_renewed_change
        form_data["state"] = "to_be_created"
        response = self.client.post(
            self._get_list_url(), form_data, format="json", **self.header
        )

        certificate = json.loads(response.content)
        form_data["state"] = "creating"
        response = self.client.patch(
            f"{self._get_list_url()}{certificate['id']}/",
            form_data,
            format="json",
            **self.header,
        )

        form_data["state"] = "errored"
        response = self.client.patch(
            f"{self._get_list_url()}{certificate['id']}/",
            form_data,
            format="json",
            **self.header,
        )

        form_data["state"] = "to_be_renewed"
        response = self.client.patch(
            f"{self._get_list_url()}{certificate['id']}/",
            form_data,
            format="json",
            **self.header,
        )
        response = self.client.patch(
            f"{self._get_list_url()}{certificate['id']}/",
            form_data,
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_200_OK)

    def test_revoked_workflow_status(self):
        """Test to change state from to_be_created to revoked"""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        obj_perm.users.add(self.user)  # pylint: disable=E1101
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))  # pylint: disable=E1101

        # Modify the form data to have a correct state value
        form_data = self.form_data_workflow
        form_data['state'] = 'to_be_created'  # Correct state value

        # Try POST with modified form data
        response = self.client.post(self._get_list_url(), form_data, format="json", **self.header)

        certificate = json.loads(response.content)
        form_data['state'] = 'creating'  # Correct state value
        response = self.client.patch(self._get_list_url() + f"{certificate['id']}/", form_data, format="json", **self.header)

        form_data['state'] = 'valid'  # Correct state value
        response = self.client.patch(self._get_list_url() + f"{certificate['id']}/", form_data, format="json", **self.header)

        form_data['state'] = 'installed'  # Correct state value
        response = self.client.patch(self._get_list_url() + f"{certificate['id']}/", form_data, format="json", **self.header)

        form_data['state'] = 'to_be_revoked'  # Correct state value
        response = self.client.patch(self._get_list_url() + f"{certificate['id']}/", form_data, format="json", **self.header)

        form_data['state'] = 'revoked'  # Correct state value
        response = self.client.patch(self._get_list_url() + f"{certificate['id']}/", form_data, format="json", **self.header)
        # Assert that the response status code is 200
        self.assertIn(response.status_code, [200])

    # moved from file : test_certificate_cont_api
    def test_content_and_url(self) -> None:
        """Test that certificate content and vault url work properly"""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        # pylint: disable=E1101
        obj_perm.users.add(self.user)
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))

        response = self.client.post(
            self._get_list_url(),
            {"cn": "truc10.com", "ca": "other", "expiration_time": "1m",
                "content": CONTENT, "vault_url": "http://mytestvault.com", "state":"to_be_created"},
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_201_CREATED)

        response = self.client.post(
            self._get_list_url(),
            {"cn": "truc11.com", "ca": "other", "expiration_time": "1m",
                "content": WRONG_CONTENT, "vault_url": "wrong_url", "state":"to_be_created"},
            format="json",
            **self.header,
        )

        self.assertHttpStatus(response, status.HTTP_400_BAD_REQUEST)


    def test_create_with_valid_service(self):
        """Test to create with the valid service value"""
        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        obj_perm.users.add(self.user)  # pylint: disable=E1101
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))  # pylint: disable=E1101

        # Modify the form data to have a correct service value
        form_data = self.form_data.copy()
        form_data['service'] = SERVICE  # Correct service value

        # Try POST with modified form data
        response = self.client.post(self._get_list_url(), form_data, format="json", **self.header)

        # Assert that the response status code is 201
        self.assertIn(response.status_code, [201])


    def test_create_with_wrong_service_value(self):
        """Test to create with the wrong state value"""
        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        obj_perm.users.add(self.user)  # pylint: disable=E1101
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))  # pylint: disable=E1101

        # Modify the form data to have an incorrect service value
        form_data = self.form_data.copy()
        form_data['service'] = WRONG_SERVICE  # Incorrect service value

        # Try POST with modified form data
        response = self.client.post(self._get_list_url(), form_data, format="json", **self.header)

        # Assert that the response status code is 400 or 422
        self.assertIn(response.status_code, [400, 422])


    def test_edit_with_wrong_service_value(self):
        """Test to edit with the wrong service value"""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        obj_perm.users.add(self.user)  # pylint: disable=E1101
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))  # pylint: disable=E1101

        # Modify the form data to have a correct service value
        form_data = self.form_data.copy()
        form_data['service'] = SERVICE  # Correct service value

        # Try POST with modified form data
        response = self.client.post(self._get_list_url(), form_data, format="json", **self.header)

        certificate = json.loads(response.content)
        form_data['service'] = WRONG_SERVICE  # Incorrect state value

        response = self.client.patch(self._get_list_url() + f"{certificate['id']}/", form_data, format="json", **self.header)

        # Assert that the response status code is 400 or 422
        self.assertIn(response.status_code, [400, 422])


    def test_edit_with_correct_service_value(self):
        """Test to edit with the wrong service value"""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        obj_perm.users.add(self.user)  # pylint: disable=E1101
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))  # pylint: disable=E1101

        # Modify the form data to have a correct service value
        form_data = self.form_data.copy()
        form_data['service'] = SERVICE  # Correct service value

        # Try POST with modified form data
        response = self.client.post(self._get_list_url(), form_data, format="json", **self.header)

        certificate = json.loads(response.content)
        form_data['service'] = 'LB'  # Correct state value

        response = self.client.patch(self._get_list_url() + f"{certificate['id']}/", form_data, format="json", **self.header)

        # Assert that the response status code is 201
        self.assertIn(response.status_code, [200, 201])


    def test_edit_with_empty_service_value(self):
        """Test to edit with the wrong service value"""

        # Add object-level permission
        obj_perm = ObjectPermission(
            name="Test permission",
            actions=["add", "change", "view"],
        )
        obj_perm.save()
        obj_perm.users.add(self.user)  # pylint: disable=E1101
        obj_perm.object_types.add(ContentType.objects.get_for_model(self.model))  # pylint: disable=E1101

        # Modify the form data to have a correct service value
        form_data = self.form_data.copy()
        form_data['service'] = ''  # Correct service value

        # Try POST with modified form data
        response = self.client.post(self._get_list_url(), form_data, format="json", **self.header)

        # Assert that the response status code is 201
        self.assertIn(response.status_code, [201])
