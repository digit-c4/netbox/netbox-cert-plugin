*** Settings ***
Library    Browser
Variables    variables.yaml
Resource    keywords.robot   

*** Variables ***
${URL_LOGIN}    ${BASE_URL_LAB}/login/
${CHECK_CA_CREATED_FULL_NAME}    ${CHECK_CA_CREATED}'${CA_DATA_NAME}']
${CA_CREATED_LINK}    ${CHECK_CA_CREATED_LINK}'${CA_DATA_NAME}']
${SUCCESS_MESSAGE_DELETE_CHECK}    Deleted certificate authority ${CA_DATA_NAME}

*** Test Cases ***
Log In To NetBox Create CA And Delete It
    [Documentation]    Log into the NetBox application with username and password.
    [Tags]    Login_and_create
    Open Browser To Login Page
    Take Screenshot
    Enter Credentials And Submit
    Open Certificate Authority Form
    Fill And Submit
    If Creation Success Delete Item

Log In Nebox Fail
    [Documentation]    Log into the NetBox application with username and password.
    [Tags]    Login_and_fail
    Open Browser To Login Page
    Take Screenshot
    Enter Credentials And Submit For Fail
    Verify Login Failed

