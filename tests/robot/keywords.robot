*** Settings ***
Library    Browser
Variables  variables.yaml  # Stores sensitive data like username and password

*** Keywords ***
Open Browser To Login Page
    New Browser    chromium    headless=True
    New Page    ${URL_LOGIN}

Enter Credentials And Submit
    # Input the username and password fields
    Click    ${USR_FREE_TXT}
    Type Text    ${USR_FREE_TXT}    ${USERNAME}
    Click     ${PSW_FREE_TXT}
    Type Text     ${PSW_FREE_TXT}    ${PASSWORD}
    # Submit the form
    Click    button[type="submit"]
    Wait For Elements State    css=.nav-link.collapsed[href='#menuOrganization']    visible    ${TIMEOUT}

Open Certificate Authority Form
    Click    ${CERTIFICATE_BUTTON}
    Wait For Elements State    ${ADD_CERTIFICATE_BUTTON}    visible    ${TIMEOUT}
    Click    ${ADD_CERTIFICATE_BUTTON}
    #Wait For Elements State    css=.nav-link.collapsed[href='#menuOrganization']    visible    ${TIMEOUT}
    Wait For Elements State    ${CA_FORM}    visible    ${TIMEOUT}
    ${current_url}=    Get Url
    Should Be True     """${current_url}""" == """${BASE_URL_LAB}${URL_LAB_ADD}"""

Fill And Submit
    Click    ${CA_NAME}
    Type Text    ${CA_NAME}    ${CA_DATA_NAME}
    Click    ${CA_BUTTON_CREATE}
    Wait For Elements State    ${CHECK_CA_CREATED_FULL_NAME}    visible    ${TIMEOUT}

Fill And Submit With Error Message
    Click    ${CA_NAME}
    Type Text    ${CA_NAME}    ${CA_DATA_NAME}
    Click    ${CA_BUTTON_CREATE}
    Wait For Elements State    ${ERROR_MSG_DUPLICATE_NAME}    visible    ${TIMEOUT}

If Creation Success Delete Item
    Click    ${CERTIFICATE_AUTHORITY_BUTTON}
    Wait For Elements State    ${CA_CREATED_LINK}    visible    ${TIMEOUT}
    Click    ${CA_CREATED_LINK}
    Wait For Elements State    ${DELETE_CA_BUTTON}    visible    ${TIMEOUT}
    Click    ${DELETE_CA_BUTTON}
    Wait For Elements State    ${CONFIRM_DELETE_CA_BUTTON}    visible    ${TIMEOUT}
    Click    ${CONFIRM_DELETE_CA_BUTTON}
    Wait For Elements State    ${SUCCESS_MSG_DELETE}    visible    ${TIMEOUT}
    ${notification_text}=    Get Text    ${SUCCESS_MSG_DELETE}
    ${trimmed_notification_text}=    Evaluate    '${notification_text.strip()}'
    Should Be Equal    ${trimmed_notification_text}    ${SUCCESS_MESSAGE_DELETE_CHECK}    The notification text does not match the expected success message.
    Log To Console    NOTIFICATION MESSAGE: ${trimmed_notification_text}
    Close Browser

Enter Credentials And Submit For Fail
    # Input the username and password fields
    Click    ${USR_FREE_TXT}
    Type Text    ${USR_FREE_TXT}    ${USERNAME_WRONG}
    Click     ${PSW_FREE_TXT}
    Type Text     ${PSW_FREE_TXT}    ${PASSWORD_WRONG}
    # Submit the form
    Click    button[type="submit"]

Verify Login Failed
    # Wait for an element that only appears post-login, we are waiting for error to show
    Wait For Elements State    css=div.alert.alert-danger    visible    ${TIMEOUT}
    Close Browser