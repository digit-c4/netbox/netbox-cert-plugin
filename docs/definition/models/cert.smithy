$version: "2.0"

namespace eu.europa.ec.snet.cert

use aws.protocols#restJson1
@title("Netbox Certificate Plugin")
@paginated(inputToken: "nextToken", 
    outputToken: "nextToken", pageSize: "pageSize")
@restJson1 
service cert {
    version: "0.0.5"
    resources: [Certificate,CertificateAuthority]
}

@pattern("^[0-9]+$")
string CertId

string Url

@pattern("^[0-9]+$")
string Id

@timestampFormat("date-time")
timestamp DatesTime

@length(max: 10)
@pattern("^\\d{4}\\-(0[1-9]|1[012])\\-(0[1-9]|[12][0-9]|3[01])$")
string Dates

