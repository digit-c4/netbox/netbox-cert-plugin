$version: "2.0"

namespace eu.europa.ec.snet.cert


@tags(["Certificate Authority"])
@documentation("Resource - Certificate Authority Table")
resource CertificateAuthority {
    identifiers: {id: CertId}
    properties: {name_ca: String, acme_url: Url, key_vault_url: Url, start_date: Dates, end_date: Dates, default_validity: Integer}
    read: GetCertificateAuthority
    create: CreateCertificateAuthority
    update: UpdateCertificateAuthority
    delete: DeleteCertificateAuthority
    list: ListCertificateAuthority
}


@tags(["Certificate Authority"])
@documentation("Show Certificate Authority based on ID")
@readonly
@http(method: "GET", uri: "/api/plugins/cert/CertificateAuthority/{id}")
operation GetCertificateAuthority {
    input := for CertificateAuthority {
        @required
        @httpLabel
        $id
    }

    output := for CertificateAuthority {
        @required
        $name_ca

        $acme_url
        $key_vault_url
        $start_date
        $end_date
        $default_validity

    } 
}



@tags(["Certificate Authority"])
@documentation("Create Certificate Authority")
@idempotent
@http(method: "POST", uri: "/api/plugins/cert/CertificateAuthority/")
operation CreateCertificateAuthority {
    input := for CertificateAuthority {
        
        @required
        $name_ca    

        $acme_url
        $key_vault_url
        $start_date
        $end_date
        $default_validity
        
    }

    output := for CertificateAuthority {
        @required
        $name_ca    

        $acme_url
        $key_vault_url
        $start_date
        $end_date
        $default_validity
    }
        errors: [NotUnique]   
}


@tags(["Certificate Authority"])
@documentation("Update a Certificate Authority")
@http(method: "PATCH", uri: "/api/plugins/cert/CertificateAuthority/{id}")
operation UpdateCertificateAuthority {
    input := for CertificateAuthority {
        @required
        @httpLabel
        $id
        @required
        $name_ca

        $acme_url
        $key_vault_url
        $start_date
        $end_date
        $default_validity
    }

        errors: [NotUnique]   
}


@tags(["Certificate Authority"])
@documentation("Delete a Certificate Authority")
@idempotent
@http(method: "DELETE", uri: "/api/plugins/cert/CertificateAuthority/{id}")
operation DeleteCertificateAuthority {
    input := for CertificateAuthority {
        @required
        @httpLabel
        $id
    }
}



@tags(["Certificate Authority"])
@documentation("List Certificate Authority")
@readonly
@http(method: "GET",uri: "/api/plugins/cert/CertificateAuthority/")
@paginated(inputToken: "nextToken", outputToken: "nextToken", pageSize: "maxResults", items: "CertificatesCA")
operation ListCertificateAuthority {
    input := for CertificateAuthority {
        @httpQuery("maxResults")
        maxResults: Integer 
        @httpQuery("nextToken")
        nextToken: String 
    }

    output := for CertificateAuthority {
        nextToken: String
        @required
        CertificatesCA: CertificateCAList
    }
}
 
@documentation("Unique constraint to prevent duplication of IDs")
@uniqueItems
list CertificateCAList {

    member: CertificateCASummary
}
@references(
[
    {resource: CertificateAuthority}
]
)
structure CertificateCASummary for CertificateAuthority {
    @required
    $id
    @required
    $name_ca

    $acme_url
    $key_vault_url
    $start_date
    $end_date
    $default_validity
}