$version: "2.0"

namespace eu.europa.ec.snet.cert


@tags(["Certificate"])
@documentation("Resource - Certificate Table")
resource Certificate {
    identifiers: {id: CertId}
    properties: {cn: String, alt_name: AltName, ca: Ca, expiration_time: String, cert_created_at: Dates, cert_expired_at: Dates, content: String, vault_url: Url, state: State, service: Service, csr: String}
    create: CreateCert
    read: GetCert
    update: UpdateCert
    delete: DeleteCert
    list: ListCert
}

@tags(["Certificate"])
@documentation("Show Certificate based on ID")
@readonly
@http(method: "GET", uri: "/api/plugins/cert/certificate/{id}")
operation GetCert {
    input := for Certificate {
        @required
        @httpLabel
        $id
    }

    output := for Certificate {
        @required
        $ca
        @required
        $cn
        @required
        $expiration_time
        @required
        $state

        $cert_created_at
        $cert_expired_at
        $alt_name
        $content
        $csr
        $vault_url
        $service

        
    } 
}



@tags(["Certificate"])
@documentation("Update Certificate based on ID")
@idempotent
@http(method: "POST", uri: "/api/plugins/cert/certificate/")
operation CreateCert {
    input := for Certificate {
        
        @required
        $ca    
        @required
        $cn
        @required
        $state

        $cert_created_at
        $cert_expired_at
        $expiration_time
        $alt_name
        $content
        $csr
        $vault_url
        $service
        
    }

    output := for Certificate {
        @required
        $ca    
        @required
        $cn
        @required
        $state

        $cert_created_at
        $cert_expired_at
        $expiration_time
        $alt_name
        $content
        $csr
        $vault_url
        $service

    }
        errors: [NotUnique]   
}

@tags(["Certificate"])
@documentation("Update a Certificate")
@http(method: "PATCH", uri: "/api/plugins/cert/certificate/{id}")
operation UpdateCert {
    input := for Certificate {
        @required
        @httpLabel
        $id
        @required
        $ca    
        @required
        $cn
        @required
        $state

        $cert_created_at
        $cert_expired_at
        $expiration_time
        $alt_name
        $content
        $csr
        $vault_url
        $service
    }

        errors: [NotUnique]   
}

@tags(["Certificate"])
@documentation("Delete a Certificate")
@idempotent
@http(method: "DELETE", uri: "/api/plugins/cert/certificate/{id}")
operation DeleteCert {
    input := for Certificate {
        @required
        @httpLabel
        $id
    }
}


@tags(["Certificate"])
@documentation("List Certificate")
@readonly
@http(method: "GET",uri: "/api/plugins/cert/certificate/")
@paginated(inputToken: "nextToken", outputToken: "nextToken", pageSize: "maxResults", items: "Certificates")
operation ListCert {
    input := for Certificate {
        @httpQuery("maxResults")
        maxResults: Integer 
        @httpQuery("nextToken")
        nextToken: String 
    }

    output := for Certificate {
        nextToken: String
        @required
        Certificates: CertificateList
    }
}
 
@documentation("Unique constraint to prevent duplication of IDs")
@uniqueItems
list CertificateList {

    member: CertificateSummary
}
@references(
[
    {resource: Certificate}
]
)
structure CertificateSummary for Certificate {
    @required
    $id
    @required
    $ca    
    @required
    $cn
    @required
    $state

    $cert_created_at
    $cert_expired_at
    $expiration_time
    $alt_name
    $content
    $csr
    $vault_url
    $service
}

@uniqueItems
list TagsList 
{
    member: Tags
}
structure Tags {
        @required
        id: Id
        url: Url
        display: String
        name: String
        slug: String
        color: String
}



enum Ca {
        LETSENCRYPT = "Lets Encrypt"
        COMMISSIGN = "CommisSign"
        GLOBALSIGN = "GlobalSign"
        OTHER = "Other"
}
enum State {
        TO_BE_CREATED = "To be created"
        CREATING = "Creating"
        ERRORED = "Errored"
        VALID = "Valid"
        TO_BE_RENEWED = "To be renewed"
        RENEWING = "Renewing"
        TO_BE_REVOKED = "To be revoked"
        REVOKED = "Revoked"
        INSTALLED = "Installed"
}

enum Service {
        WIFI = "WIFI"
        RPS = "RPS"
        RAS = "RAS"
        LB = "LB"
}

list AltName
    {
        member: String

    }
