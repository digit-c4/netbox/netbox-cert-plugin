$version: "2.0"

namespace eu.europa.ec.snet.cert

//Get error when ID not unique
@documentation("Error used for failure to garantee uniqueness of IDs")
@error("client")
structure NotUnique {
    @required
    message: String = "Unique Constraint Failed"
}