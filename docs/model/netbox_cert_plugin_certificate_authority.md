## netbox_cert_plugin_certificate_authority

**Certificate Authority definition class**

| FIELD             | TYPE                         | DESCRIPTION                                                                     |
| ----------------- | ---------------------------- | ------------------------------------------------------------------------------- |
| id                | Big (8 byte) integer         | ID                                                                              |
| ca_name           | String (up to 64)            | Friendly name: CA Name. Mandatory and Unique / Example: "GlobalSign RSA OV SSL CA 2018" |
| acme_url          | String (up to 256)           | ACME API URL. Not mandatory / Example: https://acme-v02.api.letsencrypt.org/directory or https://acme.commissign.cec.eu.int:8443/acme/opentrust-pki/directory |
| key_vault_url     | String (up to 512)           | ACME Vault URL. Not mandatory / Example: https://sam-hcavault.cec.eu.int/ui/vault/secrets/cert_manager/show/certificates/tcaaslab/3?namespace=EC/DIGIT_C4_SNET_ADMIN-ACC |
| start_date        | Date (without time)          | CA Valid From. Not Mandatory                                                    |
| end_date          | Date (without time)          | CA Valid Until. Not Mandatory / Should always be set after the Start Date       |
| default_validity  | Integer                      | Default validity period. Not mandatory / Min: 1 Day, Max: 2000 Days             |