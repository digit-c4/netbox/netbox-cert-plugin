## netbox_cert_plugin_certificate

**Certificate definition class**

| FIELD             | TYPE                         | DESCRIPTION                                                                     |
| ----------------- | ---------------------------- | ------------------------------------------------------------------------------- |
| alt_name          | Array of String (up to None) | Alt Name - Alt Name is a list of host separated by commas (e.g. alt1,alt2,alt3) |
| ca                | String (up to 32)            | Certificate Authority                                                           |
| cert_created_at   | Date (without time)          | Effective certificate's creation date                                           |
| cert_expired_at   | Date (without time)          | Effective certificate's expiration date                                         |
| cn                | String (up to 256)           | Common Name - Unique Common Name                                                |
| created           | Date (with time)             | created                                                                         |
| custom_field_data | A JSON object                | custom field data                                                               |
| expiration_time   | String (up to 2)             | Expiration time needed                                                          |
| id                | Big (8 byte) integer         | ID                                                                              |
| content           | String (no limit)            | PEM Certificate                                                                 |
| vault_url         | String (up to 256)           | URL to corresponding Vault                                                      |
| last_updated      | Date (with time)             | last updated                                                                    |
| state             | String (up to 32)            | Certificate State                                                               |
| service           | String (up to 32)(Not required)| Service - Squad where the certificate is deployed / Values: WIFI, RPS, RAS, LB|