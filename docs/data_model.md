# Cert Plugin Data Model Documentation
*v1.2*

![schema_diagram.png](https://code.europa.eu/digit-c4/netbox/netbox-cert-plugin/-/jobs/artifacts/main/raw/.project/docs/model/schema_diagram.png?job=doc-job)
* [Certificate table definition](model/netbox_cert_plugin_certificate.md)
* [Certificate Authority table definition](model/netbox_cert_plugin_certificate_authority.md)

