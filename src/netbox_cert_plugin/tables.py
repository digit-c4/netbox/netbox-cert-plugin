"""Tables definitions"""

import django_tables2 as tables
from netbox.tables import NetBoxTable, columns
from .models import Certificate, CertificateAuthority


class CertificateTable(NetBoxTable):
    """Certificate Table definition class"""

    cn = tables.Column(linkify=True)
    tags = columns.TagColumn()

    class Meta(NetBoxTable.Meta):
        """Meta class"""

        model = Certificate
        fields = (
            "pk",
            "id",
            "cn",
            "alt_name",
            "ca",
            "state",
            "expiration_time",
            "cert_created_at",
            "cert_expired_at",
            "content",
            "vault_url",
            "tags",
            "service"
        )
        default_columns = (
            "cn",
            "alt_name",
            "ca",
            "expiration_time",
            "state",
            "cert_created_at",
            "cert_expired_at",
        )

class CertificateAuthorityTable(NetBoxTable):
    """Certificate Table definition class"""

    ca_name = tables.Column(linkify=True)
    tags = columns.TagColumn()

    class Meta(NetBoxTable.Meta):
        """Meta class"""

        model = CertificateAuthority
        fields = (
            "ca_name",
            "acme_url",
            "key_vault_url",
            "start_date",
            "end_date",
            "default_validity",
        )
        default_columns = (
            "ca_name",
            "acme_url",
            "key_vault_url",
            "start_date",
            "end_date",
            "default_validity",
        )
