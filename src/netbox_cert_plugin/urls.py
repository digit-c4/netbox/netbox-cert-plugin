"""URL definitions"""

from django.urls import path
from netbox.views.generic import ObjectChangeLogView, ObjectJournalView
from netbox_cert_plugin import views, models


urlpatterns = (
    # Cert
    path("certificates/", views.CertificateListView.as_view(), name="certificate_list"),
    path(
        "certificates/add/", views.CertificateAddView.as_view(), name="certificate_add"
    ),
    path(
        "certificates/edit/", views.CertificateEditView.as_view(), name="certificate_edit"
    ),
    path(
        "certificates/import/",
        views.CertificateBulkImportView.as_view(),
        name="certificate_import",
    ),
    path(
        "certificates/delete/",
        views.CertificateBulkDeleteView.as_view(),
        name="certificate_bulk_delete",
    ),
    path("certificates/<int:pk>/", views.CertificateView.as_view(), name="certificate"),
    path(
        "certificates/<int:pk>/edit/",
        views.CertificateEditView.as_view(),
        name="certificate_edit",
    ),
    path(
        "certificates/<int:pk>/delete/",
        views.CertificateDeleteView.as_view(),
        name="certificate_delete",
    ),
    path(
        "certificates/<int:pk>/changelog/",
        ObjectChangeLogView.as_view(),
        name="certificate_changelog",
        kwargs={"model": models.Certificate},
    ),
    path(
        "certificates/<int:pk>/journal/",
        ObjectJournalView.as_view(),
        name="certificate_journal",
        kwargs={"model": models.Certificate},
    ),

    path("CertificateAuthority/", views.CertificateAuthorityListView.as_view(), name="certificateauthority_list"),
    path("CertificateAuthority/add/", views.CertificateAuthorityEditView.as_view(), name="certificateauthority_add"),
    path("CertificateAuthority/<int:pk>/edit/", views.CertificateAuthorityEditView.as_view(), name="certificateauthority_edit"),
    path("CertificateAuthority/import/", views.CertificateAuthorityBulkImportView.as_view(), name="certificateauthority_import"),
    path("CertificateAuthority/delete/", views.CertificateAuthorityBulkDeleteView.as_view(), name="certificateauthority_bulk_delete"),
    path("CertificateAuthority/<int:pk>/", views.CertificateAuthorityView.as_view(), name="certificateauthority"),
    path("CertificateAuthority/<int:pk>/delete/", views.CertificateAuthorityDeleteView.as_view(), name="certificateauthority_delete"),
    path("CertificateAuthority/<int:pk>/changelog/", ObjectChangeLogView.as_view(), name="certificateauthority_changelog", kwargs={"model": models.CertificateAuthority}),
    path("CertificateAuthority/<int:pk>/journal/", ObjectJournalView.as_view(), name="certificateauthority_journal", kwargs={"model": models.CertificateAuthority}),

)
