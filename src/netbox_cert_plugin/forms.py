"""Forms definitions"""

from django import core, forms

from netbox.forms import (
    NetBoxModelFilterSetForm,
    NetBoxModelForm,
    NetBoxModelImportForm,
)

from utilities.forms import (
    fields,
    widgets,
)

from utilities.forms.widgets import DatePicker
from utilities.forms.fields import TagFilterField
from .models import Certificate, ExpirationTimeChoices, CaChoices, CertificateAuthority, ServiceChoices
# from .models import StateChoices

ValidationError = core.exceptions.ValidationError
DatePicker = widgets.DatePicker
TagFilterField = fields.TagFilterField


class CertificateAddForm(NetBoxModelForm):
    """Certificate form definition class"""

    model = Certificate
    state = forms.CharField(
        label="Certificate state", required=False
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Set initial value for the state field
        self.initial['state'] = 'To be created'
        self.fields['state'].widget.attrs['readonly'] = True


    class Meta:
        """Meta class"""

        model = Certificate
        fields = (
            "cn",
            "alt_name",
            "ca",
            "expiration_time",
            "state",
            "cert_created_at",
            "cert_expired_at",
            "content",
            "vault_url",
            "tags",
            "service",
        )
        help_texts = {"CN": "Unique Common Name", "CA": "Valid Certificate Authority"}
        labels = {
            "cn": "CN",
            "alt_name": "Alt name",
            "ca": "CA",
            "expiration_time": "Expiration time",
            "state": "Certificate state",
            "cert_created_at": "Effective certificate's creation date",
            "cert_expired_at": "Effective certificate's expiration date",
            "service": "Service",
        }
        widgets = {
            "cert_created_at": DatePicker(),
            "cert_expired_at": DatePicker(),
        }

    def clean_state(self):
        """ Put the expected value for instance state """
        return 'to_be_created'


class CertificateEditForm(NetBoxModelForm):
    """Certificate form definition class"""

    model = Certificate
    state = forms.ChoiceField(
        label="Certificate state", choices=[("valid", "Valid"), ("to_be_renewed", "To be renewed")], required=False
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Set initial value for the state field
        if self.instance:
            # Set initial value only if the form is bound to an instance
            self.initial['state'] = 'To be renewed'


    class Meta:
        """Meta class"""

        model = Certificate
        fields = (
            "cn",
            "alt_name",
            "ca",
            "expiration_time",
            "state",
            "cert_created_at",
            "cert_expired_at",
            "content",
            "vault_url",
            "tags",
            "service",
        )
        help_texts = {"CN": "Unique Common Name", "CA": "Valid Certificate Authority"}
        labels = {
            "cn": "CN",
            "alt_name": "Alt name test",
            "ca": "CA",
            "expiration_time": "Expiration time",
            "state": "Certificate state",
            "cert_created_at": "Effective certificate's creation date",
            "cert_expired_at": "Effective certificate's expiration date",
            "content": "Certificate content",
            "vault_url": "Vault URL",
            "service": "Service",
        }
        widgets = {
            "cert_created_at": DatePicker(),
            "cert_expired_at": DatePicker(),
        }


class CertificateFilterForm(NetBoxModelFilterSetForm):
    """Certificate filter form definition class"""

    model = Certificate
    cn = forms.CharField(
        label="Common Name", max_length=256, min_length=1, required=False
    )
    alt_name = forms.CharField(
        label="Alt name", max_length=256, min_length=1, required=False
    )
    ca = forms.MultipleChoiceField(
        label="Certificate Authority", choices=CaChoices, required=False
    )
    expiration_time = forms.MultipleChoiceField(
        label="Expiration time", choices=ExpirationTimeChoices, required=False
    )
    state = forms.CharField(
        label="Certificate state", required=False, widget=forms.TextInput
    )
    cert_created_at = forms.DateField(
        label="Effective certificate's creation date", required=False, widget=DatePicker
    )
    cert_expired_at = forms.DateField(
        label="Effective certificate's expirations date",
        required=False,
        widget=DatePicker,
    )
    tag = TagFilterField(model)
    content = forms.CharField(
        label="PEM Certificate", required=False, widget=forms.TextInput
    )
    vault_url = forms.URLField(
        label="URL to corresponding vault", required=False, widget=forms.TextInput
    )
    service = forms.MultipleChoiceField(
        label="Service", choices=ServiceChoices, required=False
    )


class CertificateImportForm(NetBoxModelImportForm):
    """Certificate importation form definition class"""


    class Meta:
        """Meta class"""

        model = Certificate
        fields = (
            "cn",
            "alt_name",
            "ca",
            "expiration_time",
            "state",
            "cert_created_at",
            "cert_expired_at",
            "content",
            "vault_url",
            "service",
        )
        labels = {
            "ca": "Certificate Authority. Can be 'letsencrypt', 'commissign', 'globalsign'",
            "expiration_time": "Expiration time needed. Can be '1m', '3m', '6m', '1y', '3y'",
            "state": "Certificate status. Can be 'pending', 'valid', 'to_be_renewed'",
            "alt_name": "Alt name separated by commas, encased with double quotes"
            + '(e.g. "alt1,alt2,alt3")',
            "service": "Service. Can be 'WIFI', 'RPS', 'RAS', 'LB'",
        }

# cert auto
class CertificateAuthorityForm(NetBoxModelForm):
    """Certificate authority form definition class"""

    class Meta:
        """Meta class"""

        model = CertificateAuthority
        fields = (
            "ca_name",
            "acme_url",
            "key_vault_url",
            "start_date",
            "end_date",
            "default_validity",
        )
        help_texts = {"CN": "Unique Common Name", "CA": "Valid Certificate Authority"}
        labels = {
            "ca_name": "CA Name",
            "acme_url": "ACME API URL",
            "key_vault_url": "ACME Vault URL",
            "start_date": "CA Valid From",
            "end_date": "CA Valid Until",
            "default_validity": "Default validity",
        }
        widgets = {
            "start_date": DatePicker(),
            "end_date": DatePicker(),
        }

class CertificateAuthorityFilterForm(NetBoxModelFilterSetForm):
    """Certificate filter form definition class"""

    model = CertificateAuthority
    ca_name = forms.CharField(
        label="Common Name", max_length=256, min_length=1, required=False
    )
    acme_url = forms.CharField(
        label="Alt name", max_length=256, min_length=1, required=False
    )
    key_vault_url = forms.CharField(
        label="Certificate Authority", max_length=256, min_length=1, required=False
    )
    start_date = forms.DateField(
        label="Expiration time", widget=DatePicker, required=False
    )
    end_date = forms.DateField(
        label="Certificate state", widget=DatePicker, required=False
    )
    default_validity = forms.CharField(
        label="Effective certificate's creation date", max_length=256, min_length=1, required=False
    )
    #tag = TagFilterField(model)

class CertificateAuthorityImportForm(NetBoxModelImportForm):
    """Certificate importation form definition class"""

    class Meta:
        """Meta class"""

        model = CertificateAuthority
        fields = (
            "ca_name",
            "acme_url",
            "key_vault_url",
            "start_date",
            "end_date",
            "default_validity",
        )
        labels = {
            "ca_name": "CA Name",
            "acme_url": "ACME API URL",
            "key_vault_url": "ACME Vault URL",
            "start_date": "CA Valid From",
            "end_date": "CA Valid Until",
            "default_validity": "Default validity",
        }
