"""Filtersets definitions"""

from django_filters import filters
from netbox.filtersets import NetBoxModelFilterSet
from django.db.models import Q
from .models import Certificate, CertificateAuthority


class CertificateFilterSet(NetBoxModelFilterSet):
    """Certificate filterset definition class"""

    alt_name = filters.CharFilter(lookup_expr="icontains")

    class Meta:
        """Meta class"""

        model = Certificate
        fields = (
            "id",
            "cn",
            "alt_name",
            "ca",
            "expiration_time",
            "state",
            "cert_created_at",
            "cert_expired_at",
            "content",
            "vault_url",
            "service",
        )

    # pylint: disable=W0613
    def search(self, queryset, name, value):
        """override"""
        if not value.strip():
            return queryset
        return queryset.filter(Q(cn__icontains=value) | Q(alt_name__icontains=value))


class CertificatAuthorityFilterSet(NetBoxModelFilterSet):
    """Certificate filterset definition class"""

    alt_name = filters.CharFilter(lookup_expr="icontains")

    class Meta:
        """Meta class"""

        model = CertificateAuthority
        fields = (
            "ca_name",
            "acme_url",
            "key_vault_url",
            "start_date",
            "end_date",
            "default_validity",
        )

    # pylint: disable=W0613
    def search(self, queryset, name, value):
        """override"""
        if not value.strip():
            return queryset
        return queryset.filter(Q(ca_name__icontains=value) | Q(acme_url__icontains=value))
