"""API Serializer definitions"""

from rest_framework import serializers
from netbox.api.serializers import NetBoxModelSerializer, WritableNestedSerializer
from ..models import Certificate, CertificateAuthority

class NestedCertificateSerializer(WritableNestedSerializer):
    """Nested Certificate Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_cert_plugin-api:certificate-detail"
    )

    class Meta:
        """Meta class"""

        model = Certificate
        fields = (
            "id",
            "url",
            "cn",
            "alt_name",
            "ca",
            "expiration_time",
            "cert_created_at",
            "cert_expired_at",
            "state",
            "content",
            "vault_url",
            "service",
        )


class CertificateSerializer(NetBoxModelSerializer):
    """Certificate Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_cert_plugin-api:certificate-detail"
    )

    class Meta:
        """Meta class"""

        model = Certificate
        fields = (
            "id",
            "url",
            "cn",
            "alt_name",
            "ca",
            "expiration_time",
            "cert_created_at",
            "cert_expired_at",
            "state",
            "custom_fields",
            "created",
            "last_updated",
            "content",
            "vault_url",
            "tags",
            "service",
        )

    def validate_state(self, value):
        """
        Validate the 'state' field.
        """
        # Define a list of valid state values
        valid_states = []

        current_state = self.instance.state if self.instance else None

        if not current_state:
            valid_states = ['to_be_created']
        if current_state == 'to_be_created':
            valid_states = ['creating', 'to_be_created']
        elif current_state == 'creating':
            valid_states = ['valid', 'errored','creating']
        elif current_state == 'errored':
            valid_states = ['errored', 'to_be_renewed']
        elif current_state == 'valid':
            valid_states = ['to_be_renewed', 'valid', 'installed', 'to_be_revoked']
        elif current_state == 'to_be_renewed':
            valid_states = ['renewing', 'to_be_renewed']
        elif current_state == 'renewing':
            valid_states = ['valid', 'errored', 'renewing']
        elif current_state == 'installed':
            valid_states = ['installed', 'valid', 'to_be_revoked', 'to_be_renewed']
        elif current_state == 'to_be_revoked':
            valid_states = ['revoked', 'errored']

        if not valid_states:
            print("Found no valid state for the current state:", current_state)
            raise serializers.ValidationError(f"Found no valid state for the current state: {current_state}")

        # Check if the provided value is in the list of valid states
        if value not in valid_states:
            print("Invalid state value. Must be one of:", valid_states)
            raise serializers.ValidationError(f"Invalid state value. Must be one of: {valid_states}")

        return value

class CertificateAuthoritySerializer(NetBoxModelSerializer):
    """Certificate Serializer class"""

    url = serializers.HyperlinkedIdentityField(
        view_name="plugins-api:netbox_cert_plugin-api:certificateauthority-detail"
    )

    class Meta:
        """Meta class"""

        model = CertificateAuthority
        fields = (
            "id",
            "ca_name",
            "acme_url",
            "key_vault_url",
            "start_date",
            "end_date",
            "default_validity",
            "url",
        )
