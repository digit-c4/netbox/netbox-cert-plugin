"""API views definitions"""

from netbox.api.viewsets import NetBoxModelViewSet
from .. import filtersets, models
from .serializers import CertificateSerializer, CertificateAuthoritySerializer


class CertificateViewSet(NetBoxModelViewSet):
    """Mapping view set class"""

    queryset = models.Certificate.objects.all()
    serializer_class = CertificateSerializer
    filterset_class = filtersets.CertificateFilterSet
    http_method_names = ["get", "post", "patch", "delete", "options"]

class CertificateAuthorityViewSet(NetBoxModelViewSet):
    """Mapping view set class"""

    queryset = models.CertificateAuthority.objects.all()
    serializer_class = CertificateAuthoritySerializer
    filterset_class = filtersets.CertificatAuthorityFilterSet
    http_method_names = ["get", "post", "patch", "delete", "options"]
