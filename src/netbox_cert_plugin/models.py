"""Models definitions"""

from django.db import models
from django.db.models import Q
from django.urls import reverse
from django.contrib.postgres.fields.array import ArrayField
from django.core.validators import MaxValueValidator, MinValueValidator
from netbox.models import NetBoxModel
from utilities.choices import ChoiceSet

class CaChoices(ChoiceSet):
    """CA choices definition class"""

    key = "Certificate.ca"

    DEFAULT_VALUE = "letsencrypt"

    CHOICES = [
        ("letsencrypt", "Let's Encrypt", "yellow"),
        ("commissign", "CommisSign", "red"),
        ("globalsign", "GlobalSign", "blue"),
        ("other", "Other", "black"),
    ]


class ExpirationTimeChoices(ChoiceSet):
    """Expiration time choices definition class"""

    key = "Certificate.expiration_time"

    DEFAULT_VALUE = "1m"

    CHOICES = [
        ("1m", "1 month", "blue"),
        ("3m", "3 months", "blue"),
        ("6m", "6 months", "blue"),
        ("1y", "1 year", "blue"),
        ("3y", "3 year", "blue"),
    ]

class ServiceChoices(ChoiceSet):
    """Service choices definition class"""

    key = "Certificate.service"

    CHOICES = [
        ("WIFI", "WIFI", "blue"),
        ("RPS", "RPS", "blue"),
        ("RAS", "RAS", "blue"),
        ("LB", "LB", "blue"),
    ]


class StateChoices(ChoiceSet):
    """CA choices definition class"""

    key = "Certificate.state"

    DEFAULT_VALUE = "to_be_created"

    CHOICES = [
        ("to_be_created", "To be created", "yellow"),
        ("creating", "Creating", "green"),
        ("errored", "Errored", "red"),
        ("valid", "Valid", "blue"),
        ("installed", "Installed", "green"),
        ("to_be_renewed", "To be renewed", "black"),
        ("renewing", "Renewing", "green"),
        ("to_be_revoked", "To be revoked", "orange"),
        ("revoked", "Revoked", "red")
    ]


class Certificate(NetBoxModel):
    """Certificate definition class"""

    cn = models.CharField(
        max_length=256,
        blank=False,
        verbose_name="Common Name",
        unique=True,
        help_text="Unique Common Name",
    )
    alt_name = ArrayField(
        base_field=models.CharField(max_length=256),
        null=True,
        blank=True,
        verbose_name="Alt Name",
        help_text="Alt Name is a list of host separated by commas (e.g. alt1,alt2,alt3)",
    )
    ca = models.CharField(
        max_length=32,
        choices=CaChoices,
        default=CaChoices.DEFAULT_VALUE,
        blank=False,
        verbose_name="Certificate Authority",
    )
    expiration_time = models.CharField(
        max_length=2,
        choices=ExpirationTimeChoices,
        default=ExpirationTimeChoices.DEFAULT_VALUE,
        blank=False,
        verbose_name="Expiration time needed",
    )
    cert_created_at = models.DateField(
        blank=True, null=True, verbose_name="Effective certificate's creation date"
    )
    cert_expired_at = models.DateField(
        blank=True, null=True, verbose_name="Effective certificate's expiration date"
    )
    state = models.CharField(
        max_length=32,
        default=StateChoices.DEFAULT_VALUE,
        blank=False,
        verbose_name="Certificate State",
    )
    content = models.TextField (
        max_length=32000,
        null=True,
        blank=True,
        verbose_name="PEM Certificate",
    )
    vault_url = models.URLField(
        max_length=256,
        null=True,
        blank=True,
        verbose_name="URL to corresponding vault",
    )
    service = models.CharField(
        max_length=32,
        choices=ServiceChoices,
        null=True,
        blank=True,
        verbose_name="Certificate Service",
    )

    class Meta:
        """Meta class"""

        ordering = ["cn"]

    def __str__(self):
        return str(self.cn)

    def get_absolute_url(self):
        """override"""
        return reverse("plugins:netbox_cert_plugin:certificate", args=[self.pk])

    # Netbox doesn't allow to show different value for the API and the GUI,
    # this code is changing the API value ex: "to_be_created" into "To be created" on the interface
    def get_state_display(self):
        """Return the display value for the state."""

        state_list = StateChoices()
        output = list(filter(lambda x:self.state in x, state_list))
        if len(output) > 0:
            output = output[0][1]
        else:
            return ""

        return output

class CertificateAuthority(NetBoxModel):
    """Certificate Authority definition class"""

    id = models.AutoField(primary_key=True)

    ca_name = models.CharField(
        max_length=64,
        blank=False,
        unique=True,
        verbose_name="CA Name"
    )
    acme_url = models.CharField(
        max_length=256,
        null=True,
        blank=True,
        verbose_name="ACME API URL"
    )
    key_vault_url = models.CharField(
        max_length=512,
        null=True,
        blank=True,
        verbose_name="ACME Vault URL"
    )
    start_date = models.DateField(
        blank=True, null=True, verbose_name="CA Valid From"
    )
    end_date = models.DateField(
        blank=True, null=True, verbose_name="CA Valid Until"
    )
    # Info : Unit in Days
    default_validity = models.IntegerField(
        blank=True, null=True,
        validators=[MinValueValidator(1), MaxValueValidator(2000)]
    )
    class Meta:
        """Meta class"""

        ordering = ["ca_name"]
        verbose_name_plural = "Certificate Authorities"
        constraints = [
            models.CheckConstraint(
                check=Q(end_date__gt=models.F('start_date')),
                name='The end date must be after the start date'
            ),
        ]

    def __str__(self):
        return str(self.ca_name)

    def get_absolute_url(self):
        """override"""
        return reverse("plugins:netbox_cert_plugin:certificateauthority", args=[self.pk])
