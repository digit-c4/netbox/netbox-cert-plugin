"""Migration File"""
# pylint: disable=C0103

from django.db import migrations, models


class Migration(migrations.Migration):
    """Migration Class"""

    dependencies = [
        ('netbox_cert_plugin', '0004_alter_certificate_state'),
    ]

    operations = [
        migrations.AddField(
            model_name='certificate',
            name='content',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='certificate',
            name='vault_url',
            field=models.URLField(blank=True, max_length=256, null=True),
        ),
    ]
