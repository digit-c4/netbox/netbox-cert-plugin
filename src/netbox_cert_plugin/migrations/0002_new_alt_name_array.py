"""Migration File"""
# pylint: disable=C0103

from django.db import migrations, models
from django.contrib.postgres.fields.array import ArrayField


class Migration(migrations.Migration):
    """Migration Class"""

    initial = True

    dependencies = [("netbox_cert_plugin", "0001_initial")]

    operations = [
        migrations.AddField(
            model_name="Certificate",
            name="new_alt_name",
            field=ArrayField(models.CharField(max_length=256), blank=True, null=True),
        ),
        migrations.RunSQL(
            sql="UPDATE netbox_cert_plugin_certificate AS t "
            + "set new_alt_name=string_to_array(t.alt_name, ',')",
            reverse_sql="UPDATE netbox_cert_plugin_certificate AS t "
            + "set alt_name=array_to_string(new_alt_name, ',')",
        ),
        migrations.RemoveField(model_name="Certificate", name="alt_name"),
        migrations.RenameField(
            model_name="Certificate", new_name="alt_name", old_name="new_alt_name"
        ),
    ]
