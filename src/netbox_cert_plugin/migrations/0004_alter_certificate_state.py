"""Migration File"""
# pylint: disable=C0103

from django.db import migrations, models


class Migration(migrations.Migration):
    """Migration Class"""

    dependencies = [
        (
            "netbox_cert_plugin",
            "0003_alter_certificate_options_certificate_state_and_more",
        ),
    ]

    operations = [
        migrations.AlterField(
            model_name="certificate",
            name="state",
            field=models.CharField(default="to_be_created", max_length=32),
        ),
    ]
