"""Model views definitions"""

from netbox.views import generic
from django.utils.translation import gettext as _
from netbox_cert_plugin import models, tables, filtersets, forms


class CertificateView(generic.ObjectView):
    """Certificate view definition"""

    queryset = models.Certificate.objects.all()

class CertificateListView(generic.ObjectListView):
    """Certificate list view definition"""

    queryset = models.Certificate.objects.all()
    table = tables.CertificateTable
    filterset = filtersets.CertificateFilterSet
    filterset_form = forms.CertificateFilterForm

class CertificateEditView(generic.ObjectEditView):
    """Certificate edition view definition"""

    queryset = models.Certificate.objects.all()
    form = forms.CertificateEditForm


class CertificateAddView(generic.ObjectEditView):
    """Certificate edition view definition"""

    queryset = models.Certificate.objects.all()
    form = forms.CertificateAddForm

class CertificateBulkImportView(generic.BulkImportView):
    """Certificate bulk import view definition"""

    queryset = models.Certificate.objects.all()
    model_form = forms.CertificateImportForm

class CertificateDeleteView(generic.ObjectDeleteView):
    """Certificate delete view definition"""

    queryset = models.Certificate.objects.all()

class CertificateBulkDeleteView(generic.BulkDeleteView):
    """Certificate bulk delete view definition"""

    queryset = models.Certificate.objects.all()
    filterset = filtersets.CertificateFilterSet
    table = tables.CertificateTable

#cert authority
#-----------------------------
class CertificateAuthorityView(generic.ObjectView):
    """Certificate Authority view definition"""

    queryset = models.CertificateAuthority.objects.all()

class CertificateAuthorityListView(generic.ObjectListView):
    """CertificateAuthority list view definition"""

    queryset = models.CertificateAuthority.objects.all()
    table = tables.CertificateAuthorityTable
    filterset = filtersets.CertificatAuthorityFilterSet
    filterset_form = forms.CertificateAuthorityForm

class CertificateAuthorityEditView(generic.ObjectEditView):
    """CertificateAuthority edition view definition"""

    queryset = models.CertificateAuthority.objects.all()
    filterset = filtersets.CertificatAuthorityFilterSet
    form = forms.CertificateAuthorityForm

class CertificateAuthorityBulkImportView(generic.BulkImportView):
    """Certificate bulk import view definition"""

    queryset = models.CertificateAuthority.objects.all()
    model_form = forms.CertificateAuthorityImportForm

class CertificateAuthorityBulkDeleteView(generic.BulkDeleteView):
    """CertificateAuthority bulk delete view definition"""

    queryset = models.CertificateAuthority.objects.all()
    filterset = filtersets.CertificatAuthorityFilterSet
    table = tables.CertificateAuthorityTable

class CertificateAuthorityDeleteView(generic.ObjectDeleteView):
    """CertificateAuthority delete view definition"""

    queryset = models.CertificateAuthority.objects.all()
