"""Navigation Menu definitions"""

from extras.plugins import PluginMenuButton, PluginMenuItem, PluginMenu
from utilities.choices import ButtonColorChoices

cert_buttons = [
    PluginMenuButton(
        link="plugins:netbox_cert_plugin:certificate_add",
        title="Add",
        icon_class="mdi mdi-plus-thick",
        color=ButtonColorChoices.GREEN,
    ),
    PluginMenuButton(
        link="plugins:netbox_cert_plugin:certificate_import",
        title="Import",
        icon_class="mdi mdi-upload",
        color=ButtonColorChoices.CYAN,
    ),
]

cert_buttons_auth = [
    PluginMenuButton(
        link="plugins:netbox_cert_plugin:certificateauthority_add",
        title="Add",
        icon_class="mdi mdi-plus-thick",
        color=ButtonColorChoices.GREEN,
    ),
    PluginMenuButton(
        link="plugins:netbox_cert_plugin:certificateauthority_import",
        title="Import",
        icon_class="mdi mdi-upload",
        color=ButtonColorChoices.CYAN,
    ),
]

certItem = [
    PluginMenuItem(
        link="plugins:netbox_cert_plugin:certificate_list",
        link_text="Certificates",
        buttons=cert_buttons,
        permissions=["netbox_cert_plugin.view_certificate"],
    ),
    PluginMenuItem(
        link="plugins:netbox_cert_plugin:certificateauthority_list",
        link_text="Certificates Authorities",
        buttons=cert_buttons_auth,
        permissions=["netbox_cert_plugin.view_certificate_auth"],
    ),
]

menu = PluginMenu(
    label="Certificates",
    groups=(("CERTIFICATE", certItem),),
    icon_class="mdi mdi-certificate",
)
