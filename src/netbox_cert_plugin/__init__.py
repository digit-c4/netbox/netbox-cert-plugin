"""Netbox Plugin Configuration"""
from netbox_nms_common.plugin_config import AutoPluginConfig


class NetBoxCertConfig(AutoPluginConfig):
    """Netbox Plugin Configuration class"""

    name = __package__
    verbose_name = "Netbox Certificate"
    base_url = "cert"
    django_apps = ["django_extensions"]

# pylint: disable=C0103
config = NetBoxCertConfig
